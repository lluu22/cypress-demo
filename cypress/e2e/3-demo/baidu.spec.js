describe('Demo of searching in baidu', function () {
  // Definite a variable
  var text = 'Cypress';
  before(() => {
    cy.log('I will run before all');
  })
  beforeEach(() => {
    cy.visit('http://www.baidu.com')
  })
  // Test Case 01
  it('I search Cypress by baidu', function () {
    // Get element by id
    cy.get('#kw').type(text);
    cy.get('#kw').should('have.value', text)
    cy.url().should("contains", "baidu")
    cy.get('#kw').clear()
  })
  // Test Case 02
  it('Should search 西安交通大学 success ', function () {
    // Get element by class name
    cy.get('.s_ipt').type('西安交通大学');
    cy.get('.s_ipt').should('have.value', '西安交通大学')
  })

  afterEach(() => {
    cy.log('I will run after each');
  })
  after(() => {
    cy.log('I will run after all');
  })
})